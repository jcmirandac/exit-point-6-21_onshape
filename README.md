# Designing a traffic light with Onshape

Go to:

www.onshape.com

and sign in. Create a new document (name it traffic_light_"your_name"):

Create a new cube. The dimensions are:
----------------------------
height: 200mm
----------------------------
depth: 200mm
----------------------------
length: 200mm
----------------------------

On the center of the front face, create a circle (120mm in diameter) and extrude 25mm (look at the image to get an idea). Do the same on the top face of the block.

![](onshapetl_001.png)

After that, follow the steps to create a box from this block.

![](onshapetl_002.png)

Create another Part Studio. Now, make a block with the same dimensions, and circles on the front, top and bottom faces. Then create a box from this new block.

![](onshapetl_003.png)

Create another Part Studio. Now, make a block with the same dimensions, and circles on the front and bottom faces. Then create a box from this new block.

![](onshapetl_004.png)

When you have the three boxes. Go to the Assembly tab and insert the first part. Select all the panels from the box and use the group tool to have all the panels together.

![](onshapetl_005.png)

Insert the other next boxes and use the group tool the same way. Then use the **fastened mate** tool to assemble the first two boxes.

![](onshapetl_006.png)

Repeat the proces for the final box.

![](onshapetl_007.png)
